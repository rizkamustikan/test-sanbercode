<?php

class Assignment extends CI_Controller
{
    public function index()
    {
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('content/assignment');
        $this->load->view('templates/footer');
    }
}
