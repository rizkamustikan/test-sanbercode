<?php

class Course extends CI_Controller
{
    public function index()
    {
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('content/course/index');
        $this->load->view('templates/footer');
    }

    public function course_add()
    {
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('content/course/course_add');
        $this->load->view('templates/footer');
    }
}
