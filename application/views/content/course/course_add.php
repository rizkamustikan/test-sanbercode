<div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Course</h1>

            <div class="page-breadcrumb d-none d-sm-flex mt-1 ml-4">
              <div class="breadcrumb-item active"><a href="<?php echo base_url('dashboard')?>"><i class="fas fa-home"></i></a></div>
              <div class="breadcrumb-item active"><a href="#">Content</a></div>
              <div class="breadcrumb-item active"><a href="<?php echo base_url('content/course/index')?>">Course</a></div>
            </div>
          </div>

                <div class="card">
                  <div class="card-header">
                    <h4>Add Course</h4>
                  </div>
                  <div class="card-header">
                    <h7>Course Details</h7>
                  </div>
                  <div class="row">
                    <div class="card-body">
                      <div class="form-group">
                        <label>Title</label> <span class="error">*</span>
                        <input type="text" class="form-control" placeholder="title">
                      </div>
                      <div class="form-group">
                        <label>Start Date</label>
                        <input type="date" class="form-control">
                      </div>
                      <div class="form-group">
                        <label>Caption</label>
                        <textarea class="form-control" required=""></textarea>
                      </div>

                      
                      <div class="card-header">
                        <h7>Content Category</h7> <hr>
                      </div>
                      <div class="form-group">
                        <label>Level</label>
                        <select class="form-control">
                          <option>pilih level</option>
                          <option>easy</option>
                          <option>medium</option>
                          <option>hard</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Categories</label>
                        <select class="form-control">
                          <option>Select Categories</option>
                          <option>easy</option>
                          <option>medium</option>
                          <option>hard</option>
                        </select>

                      <div class="form-group">
                        <label>File</label>
                        <input type="file" class="form-control">
                      </div>
                      </div>
                      <div class="form-group">
                        <label>End Date</label>
                        <input type="date" class="form-control">
                      </div>
                        <div class="form-group mb-3">
                          <label>Description</label>
                          <textarea class="form-control" required=""></textarea>
                        </div>

                      <div class="card-header">
                        <h7>Current Control</h7> <hr>
                      </div>
                      <div class="form-group">
                        <label class="d-block">Privacy</label>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" checked>
                          <label class="form-check-label" for="exampleRadios1">
                            Public
                          </label>
                        </div>
                        <label>Everyone can watch your video</label>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" checked>
                          <label class="form-check-label" for="exampleRadios2">
                            Protected
                          </label>
                        </div>
                        <label>Everyone with link can watch your video</label>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" checked>
                          <label class="form-check-label" for="exampleRadios2">
                            Private
                          </label>
                        </div>
                        <label>Only someone with access can watch your video</label>
                      </div>
                      <div class="card-footer text-right">
                        <button class="btn btn-danger mr-1" type="submit">Add</button>
                        <button class="btn btn-secondary-danger" type="reset">Discard</button>
                      </div>
                      
                  </div>
        <section>
</div>
