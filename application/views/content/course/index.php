<div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Course</h1>

            <div class="page-breadcrumb d-none d-sm-flex mt-1 ml-4">
              <div class="breadcrumb-item active"><a href="<?php echo base_url('dashboard')?>"><i class="fas fa-home"></i></a></div>
              <div class="breadcrumb-item active"><a href="#">Content</a></div>
              <div class="breadcrumb-item active"><a href="<?php echo base_url('content/course/index')?>">Course</a></div>
            </div>
          </div>
            
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Cource List</h4>
                    <div class="card-header-action">
                        <div class="button">
                          <a href="<?= base_url('content/course/course_add'); ?>" class="btn btn-outline-danger"><i class="fas fa-plus"></i>Add Course</a>
                        </div>
                    </div>
                  </div>
                  <div class="card-body p-0">
                    <div class="table-responsive">
                      <table class="table table-striped" id="sortable-table">
                        <thead>
                          <tr>
                            <th>No. </th>
                            <th>Title</th>
                            <th>Organization</th>
                            <th>Status</th>
                            <th>Last Update</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                              <div class="sort-handler">
                                <i class="fas fa-th"></i>
                              </div>
                            </td>
                            <td>Create a mobile app</td>
                            <td class="align-middle">
                              <div class="progress" data-height="4" data-toggle="tooltip" title="100%">
                                <div class="progress-bar bg-success" data-width="100"></div>
                              </div>
                            </td>
                            <td><div class="badge badge-success">Completed</div></td>
                            <td>2018-01-20</td>
                            <td><a href="#" class="btn btn-secondary">Detail</a></td>
                          </tr>
                          <tr>
                            <td>
                              <div class="sort-handler">
                                <i class="fas fa-th"></i>
                              </div>
                            </td>
                            <td>Redesign homepage</td>
                            <td class="align-middle">
                              <div class="progress" data-height="4" data-toggle="tooltip" title="0%">
                                <div class="progress-bar" data-width="0"></div>
                              </div>
                            </td>
                            <td><div class="badge badge-info">Todo</div></td>
                            <td>2018-04-10</td>
                            <td><a href="#" class="btn btn-secondary">Detail</a></td>
                          </tr>
                          <tr>
                            <td>
                              <div class="sort-handler">
                                <i class="fas fa-th"></i>
                              </div>
                            </td>
                            <td>Backup database</td>
                            <td class="align-middle">
                              <div class="progress" data-height="4" data-toggle="tooltip" title="70%">
                                <div class="progress-bar bg-warning" data-width="70"></div>
                              </div>
                            </td>
                            <td><div class="badge badge-warning">In Progress</div></td>
                            <td>2018-01-29</td>
                            <td><a href="#" class="btn btn-secondary">Detail</a></td>
                          </tr>
                          <tr>
                            <td>
                              <div class="sort-handler">
                                <i class="fas fa-th"></i>
                              </div>
                            </td>
                            <td>Input data</td>
                            <td class="align-middle">
                              <div class="progress" data-height="4" data-toggle="tooltip" title="100%">
                                <div class="progress-bar bg-success" data-width="100"></div>
                              </div>
                            </td>
                            <td><div class="badge badge-success">Completed</div></td>
                            <td>2018-01-16</td>
                            <td><a href="#" class="btn btn-secondary">Detail</a></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>