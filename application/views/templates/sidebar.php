<div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="index.html">Stisla</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">St</a>
          </div>
          <ul class="sidebar-menu">
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
              <ul class="dropdown-menu">
                <li class=active><a class="nav-link" href="<?php echo base_url('dashboard')?>">General Dashboard</a></li>
              </ul>
            </li>

            <li class="menu-header">clone management</li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-cube"></i> <span>Content</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="<?php echo base_url('content/assignment')?>"><i class="far fa-circle"></i>Assignment</a></li>
                <li><a class="nav-link" href=""><i class="far fa-circle"></i>Webinar</a></li>
                <li><a class="nav-link" href=""><i class="far fa-circle"></i>Video</a></li>
                <li><a class="nav-link" href=""><i class="far fa-circle"></i>Podcast</a></li>
                <li><a class="nav-link" href=""><i class="far fa-circle"></i>Certificate</a></li>
                <li class="nav-item"><a class="nav-link" href="<?php echo base_url('content/course/index')?>"><i class="far fa-circle"></i>Course</a></li>
                <li><a class="nav-link" href=""><i class="far fa-circle"></i>Pathways</a></li>
                <li><a class="nav-link" href=""><i class="far fa-circle"></i>Voucher</a></li>
                <li><a class="nav-link" href=""><i class="far fa-circle"></i>Quiz</a></li>
                <li><a class="nav-link" href=""><i class="far fa-circle"></i>Category</a></li>
                <li><a class="nav-link" href=""><i class="far fa-circle"></i>Competencies</a></li>
              </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="far fa-user"></i> <span>Account</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="bootstrap-alert.html">Alert</a></li>
                <li><a class="nav-link" href="bootstrap-badge.html">Badge</a></li>
              </ul>
            </li>  

            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-cog"></i> <span>Setting</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="bootstrap-alert.html">Alert</a></li>
                <li><a class="nav-link" href="bootstrap-badge.html">Badge</a></li>
              </ul>
            </li>  

            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-chart-pie"></i> <span>Analytic</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="bootstrap-alert.html">Alert</a></li>
                <li><a class="nav-link" href="bootstrap-badge.html">Badge</a></li>
              </ul>
            </li>  

            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fa-light fa-aperture"></i> <span>Branding</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="bootstrap-alert.html">Alert</a></li>
                <li><a class="nav-link" href="bootstrap-badge.html">Badge</a></li>
              </ul>
            </li>  

            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="fas fa-link"></i> <span>Integrasi</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="bootstrap-alert.html">Alert</a></li>
                <li><a class="nav-link" href="bootstrap-badge.html">Badge</a></li>
              </ul>
            </li>  
        </aside>
      </div>